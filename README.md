# Python Auto-Destruct Script

This script automatically deletes files in a specified directory after they have existed for a certain period. The script also deletes itself if no files are found in the target directory. It is intended to be run periodically using a cron job.

## Requirements

- Python 3.x

## Setup

1. Ensure you have Python 3 installed on your system.

2. Clone or download this repository to your local machine.

3. Modify the script to specify the absolute path of the directory to be monitored and the deletion time threshold.

## Configuration

The script is configured through the following variables:

- `absolute_path`: The absolute path to the directory where files will be deleted.
- `delete_after`: The time in seconds after which files should be deleted. In this script, it is set to 5 minutes.
- `delete_script`: Boolean flag to determine if the script should delete itself when no files are found in the directory.

```python
absolute_path = '/home/dyilife/Dev/python_auto_destruct/delete/' # Absolute path to the directory for files deletion
delete_after = 1 * 60 * 5 # seconds - In this case this is 5 minutes
delete_script = True
```

## Usage

1. Place the script (`run.py`) in a desired directory.

2. Schedule the script to run periodically using a cron job. For example, to run the script every 5 minutes, add the following line to your crontab:

   ```sh
   */5 * * * * /usr/bin/python3 /home/dyilife/Dev/python_auto_destruct/delete/run.py
   ```

3. The script will check the specified directory every 5 minutes and delete any Python files (`.py`) that are older than the specified time threshold (`delete_after`). If no files are found in the directory, and `delete_script` is set to `True`, the script will delete itself.

## Script Details

### List Files in Directory

The script lists all files in the specified directory:

```python
files = os.listdir(absolute_path)
print(files)
```

### Delete File Function

The `delete_file` function attempts to delete a specified file and handles possible exceptions:

```python
def delete_file(file_path):
    try:
        os.remove(file_path)
        print(f"File {file_path} has been deleted successfully.")
    except FileNotFoundError:
        print(f"File {file_path} not found.")
    except PermissionError:
        print(f"Permission denied: unable to delete {file_path}.")
    except Exception as e:
        print(f"Error occurred while deleting file {file_path}: {e}")
```

### Get File Age Function

The `get_file_age` function returns the age of a file in seconds, minutes, hours, and days:

```python
def get_file_age(file_path):
    current_time = time.time()
    try:
        file_mod_time = os.path.getmtime(file_path)
    except FileNotFoundError:
        return None
    file_age_seconds = current_time - file_mod_time
    file_age_minutes = file_age_seconds / 60
    file_age_hours = file_age_minutes / 60
    file_age_days = file_age_hours / 24
    return {
        "seconds": file_age_seconds,
        "minutes": file_age_minutes,
        "hours": file_age_hours,
        "days": file_age_days
    }
```

### File Deletion Logic

The script iterates through each file in the directory, checks its age, and deletes it if it is older than the specified threshold:

```python
for f in files:
    file_extension = f.split('.')[-1]
    if file_extension == 'py':
        abs_path = absolute_path + f
        file_age = get_file_age(abs_path)
        seconds = file_age['seconds']
        if seconds > delete_after:
            delete_file(abs_path)
            print(f"File {f} is {file_age['seconds']} seconds old. Deleting...")
        else:
            remaining_time = delete_after - seconds
            print(f"File {f} is {file_age['seconds']} seconds old. Deleting in {remaining_time} seconds.")
```

### Self-Deletion Logic

If no files are found in the directory, and `delete_script` is `True`, the script deletes itself:

```python
number_of_files = len(files)
if number_of_files == 0:
    print("No files found in the directory.")
    if delete_script:
        script_name = os.path.basename(__file__)
        delete_file(current_script_path + '/' + script_name)
        print(f"Script {script_name} has been deleted successfully.")
    exit()
```

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

---

For any issues or questions, please contact stackoverflow.