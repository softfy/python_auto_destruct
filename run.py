import os
import time
import sys


absolute_path = '/home/dyilife/Dev/python_auto_destruct/delete/' # Absolute path to the directory for files deletion
delete_after = 1 * 60 * 5 # seconds - In this case this is 5 minutes
delete_script = True
script_name = os.path.basename(__file__)

#crontab command
# */5 * * * * /usr/bin/python3 /home/dyilife/Dev/python_auto_destruct/delete/run.py
# this checks every 5 minutes


current_script_path = os.path.dirname(os.path.realpath(__file__))
files = os.listdir(absolute_path)

print(files)
# files = [f for f in os.listdir('.') if os.path.isfile(f)]

def delete_file(file_path):
    try:
        os.remove(file_path)
        print(f"File {file_path} has been deleted successfully.")
    except FileNotFoundError:
        print(f"File {file_path} not found.")
    except PermissionError:
        print(f"Permission denied: unable to delete {file_path}.")
    except Exception as e:
        print(f"Error occurred while deleting file {file_path}: {e}")

def get_file_age(file_path):
    # Get the current time
    current_time = time.time()
    
    # Get the last modification time of the file
    try:
        file_mod_time = os.path.getmtime(file_path)
    except FileNotFoundError:
        return None
    
    # Calculate the age of the file
    file_age_seconds = current_time - file_mod_time
    file_age_minutes = file_age_seconds / 60
    file_age_hours = file_age_minutes / 60
    file_age_days = file_age_hours / 24
    
    return {
        "seconds": file_age_seconds,
        "minutes": file_age_minutes,
        "hours": file_age_hours,
        "days": file_age_days
    }





for f in files:
    file_extension = f.split('.')[-1]
    if file_extension == 'py':
        abs_path = absolute_path + f
        file_age = get_file_age(abs_path)
        seconds = file_age['seconds']
        if seconds > delete_after:
            delete_file(abs_path)
            print(f"File {f} is {file_age['seconds']} seconds old. Deleting...")
        else:
            remaining_time = delete_after - seconds
            print(f"File {f} is {file_age['seconds']} seconds old. Deleting in {remaining_time} seconds.")


number_of_files = len(files)

if number_of_files == 0:
    print("No files found in the directory.")
    if delete_script:
        script_name = os.path.basename(__file__)
        delete_file(current_script_path+'/'+script_name)
        print(f"Script {script_name} has been deleted successfully.")
    exit()